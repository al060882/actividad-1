import java.util.*;

/**
 *
 * @author Santiago
 */
public class programa01 {
    public static void main(String[] args) {
        Scanner n1 = new Scanner(System.in);
        Scanner n2 = new Scanner(System.in);
        //Datos primarios
        System.out.print("Introduzca el N° de lados: ");
        int lados = n1.nextInt();
        System.out.println("Introduzca el lado inicial");
        double lado_i = n2.nextDouble();
        
        for(int i = 0; i<=20; i++){
            //Formulas
            double C = lado_i/(2);
            double D = Math.sqrt(1-(C*C));
            double E = 1 - D;
            double F = Math.sqrt((C*C)+(E*E));
            double G = lados*lado_i;
            double H = G/2;
        
            System.out.println("A: "+lados);
            System.out.println("B: "+lado_i);
            System.out.println("C: "+C);
            System.out.println("D: "+D);
            System.out.println("E: "+E);
            System.out.println("F: "+F);
            System.out.println("G: "+G);
            System.out.println("H: "+H);
            
            lado_i = F;
            lados = lados *2;
        }
    }
}

